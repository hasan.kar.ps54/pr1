import numpy as np
def average(v,X):
    N = len(X)
    summ = 0
    for i in range(1,N,1):
        summ += v[i]*X[i]
    return summ
N = 2
v=np.zeros(N+1)
Rho=np.zeros(N+1)


v[2] = 0.2
v[1] = 1.-v[2]
Rho[1] = 2.65
Rho[2] = 0.8
print(average(v,Rho))