import numpy as np
import def_average as df

N = 2
v = np.zeros(N+1)
Rho = np.zeros(N+1)
Vp = np.zeros(N+1)

v[2] = 0.2
v[1] = 1.-v[2]  

Rho[1] = 2.65
Rho[2] = 0.8

Vp[1] = 6.2
Vp[2] = 1.0 
print(df.average(v,Rho))
print(df.average(v,Vp))