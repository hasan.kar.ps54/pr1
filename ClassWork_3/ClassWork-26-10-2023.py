# Тип данных кортеж
# Это неизменяемый список
# Пример 
tuple_1 = ()
print(tuple_1)
# Кортеж с целыми числами
my_tuple = (10, 222, 35)
print(my_tuple)
# Кортеж с разными типами данных 
my_tuple_data = (2055, "Sandstone", 324.5)
print(my_tuple_data)
# Вложенный кортеж
my_tuple_inc = (232, "data", [2,4,3])
print(my_tuple_inc)
###################
tuple_pack = 234, 34, 234.4, "sldf"
print(type(tuple_pack))
a,b,c,d = tuple_pack
print(a)
print(b)
print(c)
print(d)
# Можно создать кортеж с одним элементом
# строка
my_tuple = ("Hello")
print(type(my_tuple))
# кортеж 
my_tuple = ("hello",)
print(type(my_tuple))
# можно и без скобок
my_tuple = "hello",
print(type(my_tuple))
#########################
my_tuple = ("c", "o","d","e","h","i", "c","k")
print(my_tuple[0])
print(my_tuple[5])
# Вложенный кортеж 
my_tuple = ("ldsflds", [2,3,5,2,2])
# Вложенное индексирование
print(my_tuple[0][2])
print(my_tuple[1][2])
###########################################
my_tuple = (3,2,1,[4,2])
# Будет ошибка my_tuple[1] = 9 
# Можно изменять списковые элементы кортежа
my_tuple[3][1] = 7575
print(my_tuple)
#######################################
rock_tuple = ("s","a","n","d","s","t","o","n","e")
# Количество встреч в кортеже
print(rock_tuple.count("s"))
# Индекс буквы "d" в кортеже
print(rock_tuple.index("d"))
# Проверка на вхождение в кортеже
my_tuple = ("d", "c","s")
print("d" in my_tuple)
print("р" in my_tuple)
################
for name in my_tuple:
    print("Буква ",name)
#########################
# Оператор extend
A = [5,25,"sldkfjk"]
B = (4,232.0, "asldfj")
A.extend(B)
print(A)
# B.extend(A) error
# print(B) error
###############################
# Тип данных словарь
# Создание пустого словаря
# my_new_dict = dict()
# another_dict = {"string_key": "the value", 2:}
list_one = ["one", "two", "three","four", "five"]
list_two = [1,2,3,4,5]
the_dict = {}
for k, v in zip(list_one, list_two):
    the_dict[k] = v + 1
print(the_dict["one"])
print(the_dict["two"])
print(the_dict.keys())
print(the_dict.values())
print(the_dict)
# 
list_one = ["one", "two", "three","four", "five"]
list_two = [1,2,3,4,5]
the_dict = dict(zip(list_one, list_two))
print(the_dict)
###################
the_dict = {0:"A",1:"B",2:"C"}
let_a=the_dict[0]
let_b = the_dict[1]
################
the_dict={}
the_dict["my_key"] = "some value"
print(the_dict)
the_dict["my_another_key"] = 43
print(the_dict)
the_dict["my_key"] = "yet another key"
print(the_dict)


print(the_dict)
#####################
my_dict = {"a":1, "b":2}
print("c" in my_dict)
print("a" in my_dict)
if not my_dict.get("A",None):
    print("EOF")
a=my_dict.get("A",None)
print(a)


