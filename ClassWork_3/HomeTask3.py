# Импортируем библиотеку регулярных выражений 
import re
# Импортируем библиотеку работы с xlsx файлом
import xlsxwriter as xl
# Импортируем нашу переменную S
from Careless_file import S
# Создаем файл xlsx
workbook = xl.Workbook('Sorted_file.xlsx')
# В нем создаем лист
worksheet = workbook.add_worksheet()
# Переменные для работы с Excel файлом
row, col = 0, 0
# re.findall(pattern, string) - находит в строке string все непересекающиеся шаблоны pattern;
# \b - означает начало или конец слова; слева не буква 
# \d+ - означает число одно или более
# [2] - означает поиск данного числа
# [3-7] - означает поиск числа в заданном диапозоне
# \. - далее следует точка
# matchP/matchV будет переменной типа list, хранящяя в себе переменные подходящие под заданный паттерн
matchP = re.findall(r"\b[2]\.\d+\b", S)
matchV = re.findall(r"\b[4-7]\.\d+\b", S)
# Name - список, хранящий названия столбцов
Name = ["Плотность", "Скорость продольная", "Скорость поперечная"]
# Цикл для запонения xlsx файла столбцами из списка Name
for i in Name:
        worksheet.write(row,col, str(i))
        col += 1
row, col = 1, 0
for i in matchP:
# Запись в Excel файл найденных значений плотности
        worksheet.write(row,col,float(i))
        row += 1
row, col = 1, 1
# Запись в Excel файл найденных значений скорости поперечных и продольных волн
for i in matchV:
# Запись в Excel файл отфильтрованых данных
        worksheet.write(row,col,float(i))
        worksheet.write(row,col + 1,float(i)/2)
        row += 1
# Закрываем Excel файл
workbook.close()