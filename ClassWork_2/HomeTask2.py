# Импортируем библиотеку регулярных выражений 
import re
# Импортируем библиотеку работы с xlsx файлом
import xlsxwriter as xl
# Импортируем нашу переменную S
from Careless_file import S
# Создаем файл xlsx
workbook = xl.Workbook('Sorted_file.xlsx')
# В нем создаем лист
worksheet = workbook.add_worksheet()
# Переменные для работы с Excel файлом
col, row = 0, 0
# re.findall(pattern, string) - находит в строке string все непересекающиеся шаблоны pattern;
# \b - означает начало или конец слова; слева не буква 
# \d+ - означает число одно или более
# \. - далее следует точка
# match будет переменной типа list, хранящяя в себе переменные подходящие под заданный паттерн
match = re.findall(r"\b\d+\.\d+\b", S)
# Цикл для фильтрации плотности и скорости, так как мы знаем, что значения плотности находятся в интервале от 0 до 3
for i in match:
    enum = float(i)
    if 2 <= enum < 3:
# Запись в Excel файл отфильтрованых данных
        worksheet.write(row,col,enum)
        row += 1
# Закрываем Excel файл
workbook.close()