# Если человек работает меньше 6 часов это недоработка
# Если болше 10 часов - переработка
# Между нормальный рабочий день
# 1 Вариант
# time = int(input())
# low_time = 6 
# high_time = 10 
# if time < low_time:
#     print("Недоработка")
# elif time > high_time:
#     print("Переработка")
# else:
#     print("Нормальный рабочий день")

# 2 Вариант
# time = int(input())
# low_time = 6 
# high_time = 10 
# a = 0
# if time < low_time:
#     print("Недоработка")
#     a = 1
# if time > high_time:
#     a = 1 
#     print("Переработка")
# if(a==0):
#     print("Нормальный рабочий день")

# 3 Вариант
# time = int(input())
# low_time = 6 
# high_time = 10 
# strl = "Нормальный рабочий день"
# if time < low_time:
#     strl = "Недоработка"
# if time > high_time:
#     strl = "Переработка"
# print(strl)

# 4 Вариант
# low_time, high_time, time = (int(input()) for i in range(3))
# strl = "Нормальный рабочий день"
# if time < low_time:
#     strl = "Недоработка"
# if time > high_time:
#     strl = "Переработка"
# print(strl)