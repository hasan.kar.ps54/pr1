# Функция пористости
def phi(p):
        pm = 2.71
        pf = 1
        fi = abs((pm-p)/(pm-pf))
        return fi

# Функция вычисления допустимых границ для скоростей упругих волн
def v_borders(fi,rock):
        K_m = 75.36
        Nu_m = 30.41
        rho = 2.71
        K_f = 2.82
        Nu_f = 10**(-6)
        K_v = (1-fi)*K_m+fi*K_f
        Nu_v = (1-fi)*Nu_m+fi*Nu_f
        K_r = ((1-fi)/K_m+fi/K_f)**(-1)
        Nu_r = ((1-fi)/Nu_m+fi/Nu_f)**(-1)
        V_v_p = ((K_v+(4/3)*Nu_v)/rock)**(1/2)
        V_v_s = (Nu_v/rock)**(1/2)
        V_r_p = ((K_r+(4/3)*Nu_r)/rock)**(1/2)
        V_r_s = (Nu_r/rock)**(1/2)
        return V_v_p, V_v_s, V_r_p, V_r_s

