__Итоговый код имеет вид:__
```python
# Импортируем библиотеку регулярных выражений 
import matplotlib.pyplot as plt
import numpy as np
import matplotlib as mpl
import re
import pandas as pd
# Функции импортируем из другого файла
from func import phi, v_borders
# Импортируем библиотеку работы с xlsx файлом
import xlsxwriter as xl
# Импортируем нашу переменную S
from Careless_file import S
# Создаем файл xlsx
workbook = xl.Workbook('Sorted_file.xlsx')
# В нем создаем лист
worksheet = workbook.add_worksheet()
# Переменные для работы с Excel файлом
row, col = 0, 0
# re.findall(pattern, string) - находит в строке string все непересекающиеся шаблоны pattern;
# \b - означает начало или конец слова; слева не буква 
# \d+ - означает число одно или более
# [2] - означает поиск данного числа
# [3-7] - означает поиск числа в заданном диапозоне
# \. - далее следует точка
# matchP/matchV будет переменной типа list, хранящяя в себе переменные подходящие под заданный паттерн
matchP = re.findall(r"\b[2-3]\.\d+\b", S)
matchV = re.findall(r"\b[4-7]\.\d+\b", S)
matchALL= re.findall(r"\b(\d+\.\d+)\b", S)
# Name - список, хранящий названия столбцов
Name = ["Пористость", "Vp", "Vs","Плотность","Check_Vp","Check_Vs"]
# Цикл для запонения xlsx файла столбцами из списка Name
for i in Name:
        worksheet.write(row,col, str(i))
        col += 1
row, col = 1, 3
for i in matchP:
# Запись в Excel файл найденных значений плотности
        worksheet.write(row,col,float(i))
        row += 1
row, col = 1, 1
# Запись в Excel файл найденных значений скорости поперечных и продольных волн
for i in matchV:
# Запись в Excel файл отфильтрованых данных
        worksheet.write(row,col,float(i))
        worksheet.write(row,col + 1,float(i)/2)
        row += 1
# Закрываем Excel файл
row, col = 1, 4
for i in range(len(matchALL)):
    val = float(matchALL[i])
    if 2<= val <3:
        # Расчет пористости
        g = phi(val)
        worksheet.write(row,col-4,g)
        # Расчет check-ов
        V_v_p, V_v_s, V_r_p, V_r_s = v_borders(g,val)
        if float(matchALL[i-1])>V_v_p:
                worksheet.write(row,col,1)
        if float(matchALL[i-1])<V_r_p:
                worksheet.write(row,col,-1)
        if V_v_p >= float(matchALL[i-1]) >= V_r_p:
                worksheet.write(row,col,0)
        if float(matchALL[i-1])/2 > V_v_s:
                worksheet.write(row,col+1,1)
        if float(matchALL[i-1])/2 < V_r_s:
                worksheet.write(row,col+1,-1)
        if V_v_s >= float(matchALL[i-1])/2 >= V_r_s:
                worksheet.write(row,col+1,0)
        row += 1
row, col = 1, 5
workbook.close()

# Файл в csv
f = pd.read_excel('Sorted_file.xlsx')
f.to_csv('Sorted_file.csv', index=False)

# Файл в txt
with open('Sorted_file.txt', 'w') as file:
    pd.read_excel('Sorted_file.xlsx').to_string(file, index=False)


# Парсинг и вывод csv
read_csv = pd.read_csv('Sorted_file.csv')
print(read_csv)

# Парсинг и вывод xlsx
read_xlsx = pd.read_excel('Sorted_file.xlsx')
print(read_xlsx)

# Парсинг и вывод txt
read_txt = pd.read_fwf('Sorted_file.txt')
print(read_txt)

# Загрузка данных из Excel-файла
df = pd.read_excel('Sorted_file.xlsx')

# Получение списка названий всех столбцов в DF
column_names = df.columns.tolist()

# Количество графиков
num_plots = len(column_names)

# Количество строк и столбцов подграфиков
num_rows = 2
num_cols = 2

# Создание фигуры и подграфиков
fig, axes = plt.subplots(num_rows, num_cols, figsize=(10, 10))

# Построение графиков
for i in range(num_rows):
    for j in range(num_cols):
        idx = i * num_cols + j
        if idx < num_plots:
            x_column = column_names[idx]
            axes[i, j].plot(df[x_column], marker='o', linestyle='-')
            axes[i, j].set_title(f'График {x_column}')
            axes[i, j].set_xlabel('Индекс')
            axes[i, j].set_ylabel('Значение')

plt.tight_layout()
plt.show()

```
## Результат выполнения программы:
![sorted](pictures/terminalpaste.png)
## Необходимые файлы хранятся в папке HomeTask4